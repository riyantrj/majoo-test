import 'package:http/http.dart' as http;
import 'package:majoo_test/db/personHelper.dart';
import 'dart:convert' as convert;

import 'package:majoo_test/model/person.dart';


class Api{
  Future req(url) async{
    var response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      var jr = convert.jsonDecode(response.body);
      return jr;
    }else{
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }

  }

  dataAwal() async{
    var api = new Api();
    var r = await api.req('https://swapi.dev/api/people/');
    var re = convert.jsonEncode(r['results']);
    print(convert.jsonDecode(re));
    var url = '';
    while(true){
        var re = convert.jsonEncode(r['results']);
        var res = convert.jsonDecode(re);
        for(var i = 0; i<res.length; i++){
          Orang o = Orang.fromMap(res[i]);
          print('saving'+o.name);
          Person().insert(o);
          print('saved');
        }
        if(r['next'] != null){
          url = r['next'];
          print(url);
          r = await api.req(url);
        }else{
          print('Break');
          break;
        }
    }
  }
}