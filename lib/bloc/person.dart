
import 'dart:async';

class PersonBloc {
  final ctr = StreamController();

  Stream get loadStream => ctr.stream;

  void list(){
    ctr.add(1);
  }

  void grid(){
    ctr.add(2);
  }

  dispose(){
    ctr.close();
  }
}