
import 'package:flutter_bloc/flutter_bloc.dart';

class PersonCubit extends Cubit<int>{
  // int page;
  PersonCubit() : super(0);
  void list(){
    // emit(state);
    emit(state+1);
  }
  void grid(){
    emit(state-1);
  }
}