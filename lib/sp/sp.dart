
import 'package:shared_preferences/shared_preferences.dart';

class SP{
  cache() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var id = sharedPreferences.get('id');
    if(id == null){
      return null;
    }else{
      return id;
    }
  }

  void addChache(v) async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('id', v.toString());

  }

  void hapus() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('id');
  }
}