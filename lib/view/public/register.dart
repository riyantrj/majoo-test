
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majoo_test/db/userHelper.dart';
import 'package:majoo_test/model/user.dart';
import 'package:majoo_test/sp/sp.dart';
import 'package:majoo_test/view/public/home.dart';
import 'package:majoo_test/view/shared/alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';


class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  Map data = new Map();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text("Register"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [

                TextFormField(
                  // keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Masukkan Nama'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Nama';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['nama'] = value;
                  },
                ),

                TextFormField(
                  // keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Masukkan Username'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Username';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['username'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      labelText: 'password'
                  ),
                  validator: (v) {
                    if(v == null){
                      return 'Harap isi Password';
                    }

                    if(v.length < 5){
                      return 'Harap Buat Password dengan panjang lebih dari 5';
                    }

                    return null;
                  },
                  onChanged: (v){
                    data['password'] = v.toString();
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      labelText: 'Password Confirmation'
                  ),
                  validator: (v) {
                    if(v == null){
                      return 'Harap isi Kembali Password';
                    }

                    if(data['password'] != v.toString()){
                      return 'Password Tidak Sama';
                    }

                    return null;
                  },

                ),
                ElevatedButton(
                  onPressed: () async{

                    if(_formKey.currentState!.validate()){
                      print(data);
                      Pengguna pdata = Pengguna(
                          data['username'], data['nama'], data['password']
                      );
                      try{
                        int id = await User().insert(pdata);
                        var sp = SP();
                        sp.addChache(id);
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Home()));
                      }on DatabaseException catch(e){
                        Alert("Error {$e}").show(context);
                      }

                    }


                  },
                  child: Text("Register"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
