import 'package:flutter/material.dart';
import 'package:majoo_test/db/userHelper.dart';
import 'package:majoo_test/sp/sp.dart';
import 'package:majoo_test/view/public/register.dart';
import 'package:majoo_test/view/shared/alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import 'home.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  Map data = new Map();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(10.0)),
            Text(
              'Majoo',
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold),
            ),
            Padding(padding: EdgeInsets.all(8.0)),
            Container(
              color: Colors.white30,
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Container(
                    width: 350.0,
                    child: TextField(
                      onChanged: (v) {
                        data['username'] = v;
                      },
                      decoration: InputDecoration(
                          labelText: 'Username',
                          prefixIcon: Icon(Icons.perm_identity)),
                    ),
                  ),
                  Container(
                    width: 350.0,
                    child: TextField(
                      obscureText: true,
                      onChanged: (v) {
                        data['password'] = v;
                      },
                      decoration: InputDecoration(
                          labelText: 'Password', prefixIcon: Icon(Icons.lock)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(8.0)),
                  Container(
                    width: 350.0,
                    child: ElevatedButton(
                        onPressed: () async{
                          try {
                            var id = await User()
                                .login(data['username'], data['password']);
                            print(id);
                            if(id != null){
                              var sp = SP();
                              sp.addChache(id.toString());
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Home()));
                            }else{
                              Alert('Maaf Username Atau Password Salah').show(context);
                            }
                          } on DatabaseException catch (e) {
                            Alert("Error {$e}").show(context);
                          }

                        },
                        child: Text('Login')),
                  ),
                  Padding(padding: EdgeInsets.all(8.0)),
                  Container(
                    width: 350.0,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.orange),
                        onPressed: () async{
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>Register()));
                        },
                        child: Text('Register')),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
