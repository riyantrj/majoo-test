import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_test/cubit/personCubit.dart';
import 'package:majoo_test/view/partial/beranda.dart';
import 'package:majoo_test/view/partial/favorite.dart';
import 'package:majoo_test/view/partial/profil.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int selected = 0;

  List<Widget> tampilan = <Widget>[
    Beranda(),
    Favorite(),
    Profil(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main'),
        backgroundColor: Colors.amber,
      ),
      body: BlocProvider(
          create: (_) => PersonCubit(),
          child: tampilan.elementAt(selected),),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.library_add), label: 'Favorit'),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), label: 'Profil'),
        ],
        currentIndex: selected,
        selectedItemColor: Colors.amber,
        onTap: (i) {
          setState(() {
            selected = i;
          });
        },
      ),
    );
  }
}
