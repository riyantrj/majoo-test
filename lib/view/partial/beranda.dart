import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majoo_test/api/api.dart';
import 'package:majoo_test/cubit/personCubit.dart';
import 'package:majoo_test/db/personHelper.dart';
import 'package:majoo_test/view/partial/formPerson.dart';
import 'package:majoo_test/view/public/home.dart';
import 'package:majoo_test/view/shared/alert.dart';
import '../shared/buatgridview.dart';
import '../shared/buatlistview.dart';

class Beranda extends StatefulWidget {
  @override
  _BerandaState createState() => _BerandaState();
}


class _BerandaState extends State<Beranda> {
  var c = 0;
  var search = '';
  String sort = '';
  List<String> dropDown = <String>["ASC", "DESC"];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            TextButton(
                onPressed: () {
                  setState(() {
                    c = 1;
                  });
                },
                child: Icon(Icons.list)),
            TextButton(
                onPressed: () {
                  setState(() {
                    c = 0;
                  });
                },
                child: Icon(Icons.grid_view)),
            Expanded(
              child: TextField(
                onChanged: (value) {
                  setState(() {
                    search = value;
                  });
                },
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                ),

              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text('Sort'),
            ),
            DropdownButton<String>(
                underline: Container(),
                icon: Icon(Icons.sort,color: Colors.black),
                items: dropDown.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (v) {
                  setState(() {
                    sort = v ?? 'ASC';
                  });;
                },),
          ],
        ),
        Expanded(
          child: FutureBuilder(
              future: Person().getdata(sort: sort),
              builder: (context, AsyncSnapshot data) {
                if (data.hasData) {
                  var d = data.data;

                  return Scaffold(
                      floatingActionButton: FloatingActionButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FormPerson()));
                        },
                        child: Icon(Icons.add),
                      ),
                      body:

                           c == 1
                              ? Listviewbuilder(d,search.toLowerCase())
                              : Gridviewbuilder(d,search.toLowerCase()),
                      );
                }else if(data.connectionState == ConnectionState.waiting){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {

                  return Center(
                    child: ElevatedButton(
                      onPressed: () async{
                        Alert('Sedang Mengambil Data').wait(context);
                        await Api().dataAwal();
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Home()));
                      },
                      child: Text('Data Awal'),
                    ),
                  );
                }
              }),
        ),
      ],
    );
  }
}
