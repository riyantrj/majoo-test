import 'package:flutter/material.dart';
import 'package:majoo_test/db/personHelper.dart';
import 'package:majoo_test/model/person.dart';
import 'package:majoo_test/view/public/home.dart';

class EditFormPerson extends StatelessWidget {
  Orang o;

  EditFormPerson(this.o);

  final _formKey = GlobalKey<FormState>();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tambahkan Data Person')),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Nama'),
                  initialValue: o.name,
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Nama';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.name = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Height'),
                  initialValue: o.height,
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Height';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.height = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Mass'),
                  initialValue: o.mass,
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Mass';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.mass = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Hair Color'),
                  initialValue: o.hair_color,
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Hari Color';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.hair_color = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.eye_color,
                  decoration: InputDecoration(labelText: 'Masukkan Eye Color'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Eye Color';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.eye_color = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.birth_year,
                  decoration: InputDecoration(labelText: 'Masukkan Birth Year'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Birth Year';
                    }
                    return null;
                  },
                  onChanged: (value) {
                   o.birth_year = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.gender,
                  decoration: InputDecoration(labelText: 'Masukkan Gender'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Gender';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.gender = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.homeworld,
                  decoration: InputDecoration(labelText: 'Masukkan Homeworld'),

                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Homeworld';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.homeworld = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.films,
                  decoration: InputDecoration(labelText: 'Masukkan Films'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Films';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.films = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.species,
                  decoration: InputDecoration(labelText: 'Masukkan Species'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Species';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.species = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.vehicles,
                  decoration: InputDecoration(labelText: 'Masukkan Vehicles'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan vehicles';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.vehicles = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  initialValue: o.starship,
                  decoration: InputDecoration(labelText: 'Masukkan Starship'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Starship';
                    }
                    return null;
                  },
                  onChanged: (value) {
                   o.starship = value;
                  },
                ),
                TextFormField(
                  initialValue: o.url,
                  decoration: InputDecoration(labelText: 'Masukkan Url'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Starship';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    o.url = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                ElevatedButton(
                  onPressed: () async{

                    if(_formKey.currentState!.validate()){
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Icon(Icons.warning),
                              content: Text('Apakah Anda Yakin Merubah Data Ini?'),
                              actions: [
                                ElevatedButton(
                                  child: Text('Tidak'),
                                  onPressed: () async {
                                    Navigator.pop(context);
                                  },
                                ),
                                ElevatedButton(
                                  style:
                                  ElevatedButton.styleFrom(primary: Colors.red),
                                  child: Text('Ya'),
                                  onPressed: () async {
                                    var now = DateTime.now().toString();
                                    o.edited = now;
                                    // Orang or = Orang.fromMap(data);
                                    await Person().update(o);
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>Home()));
                                  },
                                )
                              ],
                            );
                          });

                    }
                  },
                  child: Text("Rubah Data"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
