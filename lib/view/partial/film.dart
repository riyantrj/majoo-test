import 'package:flutter/material.dart';
import 'package:majoo_test/api/api.dart';
import 'package:majoo_test/model/film.dart';
import 'package:intl/intl.dart';


class Film extends StatelessWidget {
  final url;
  Film(this.url);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Api().req(url),
        builder: (context, AsyncSnapshot data){

          if(data.hasData){
            FilmModel film = FilmModel.fromMap(data.data);
            DateTime tgl =  DateFormat('yyyy-MM-dd').parse(film.release_date);
            DateFormat formater = DateFormat('yyyy-MM-dd');
            String tanggal = formater.format(tgl);
            return Scaffold(
              appBar: AppBar(title: Text('${film.title}'),),
              body: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: ListView(
                    children: [
                      ListTile(
                        leading: Icon(Icons.title),
                        title: Text('${film.title}'),
                      ),
                      ListTile(
                        leading: Icon(Icons.confirmation_number),
                        title: Text('Episode ${film.episode_id}'),
                      ),
                      ListTile(
                        leading: Icon(Icons.person),
                        title: Text('Director ${film.director}'),
                      ),
                      ListTile(
                        leading: Icon(Icons.person),
                        title: Text('Producer ${film.producer}'),
                      ),
                      ListTile(
                        leading: Icon(Icons.date_range),
                        title: Text('$tanggal'),
                      ),
                      ListTile(
                        leading: Icon(Icons.book),
                        title: Text('${film.opening_crawl}'),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
          return Container();
        });
  }
}
