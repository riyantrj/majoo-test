import 'package:flutter/material.dart';
import 'package:majoo_test/db/favoriteHelper.dart';
import 'package:majoo_test/db/personHelper.dart';
import 'package:majoo_test/model/favorite.dart';
import 'package:majoo_test/model/person.dart';

import 'detailview.dart';

class Favorite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: FavoriteHelper().getdata(),
        builder: (context, AsyncSnapshot data) {
          if (data.hasData) {
            var d = data.data;

            return ListView.builder(
                itemCount: d.length,
                itemBuilder: (context, index) {
                  FavoriteModel f = FavoriteModel.fromMap(d[index]);

                  return FutureBuilder(
                    future: f.getnama(f.url),
                    builder: (contex, AsyncSnapshot data) {
                      // print(data.data);

                      if (data.hasData) {
                        Orang o = Orang.fromMap(data.data);
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Detailveiw(o)));
                          },
                          child: ListTile(
                            leading: Icon(Icons.person),
                            // trailing: GestureDetector(
                            //     onTap: () {
                            //       print('tap Disini');
                            //     },
                            //     child: Icon(Icons.favorite)),
                            title: Text("Url ${o.name}"),
                            // subtitle: Text("Gender: "),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  );
                });
          } else {
            return Center(child: Container(
              child: Text("Data Belum Ada, Favoritkan Salah Satu Aktor Di beranda"),
            ));
          }
        });
  }
}
