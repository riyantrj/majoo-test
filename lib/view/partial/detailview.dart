import 'package:flutter/material.dart';
import 'package:majoo_test/db/personHelper.dart';
import 'package:majoo_test/model/favorite.dart';
import 'package:majoo_test/model/person.dart';
import 'package:majoo_test/sp/sp.dart';
import 'package:majoo_test/view/partial/editFormPerson.dart';
import 'package:majoo_test/view/public/home.dart';
import 'package:majoo_test/view/shared/alert.dart';
import 'package:majoo_test/db/favoriteHelper.dart';
import 'package:majoo_test/view/shared/buatlistbutton.dart';

import 'film.dart';

class Detailveiw extends StatelessWidget {
  final Orang o;

  Detailveiw(this.o);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: ListTile(
          title: Text(
            'Detail ${o.name}',
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
          trailing: GestureDetector(
              onTap: () async {
                var id = await SP().cache();
                var f = FavoriteModel(id, o.url);
                FavoriteHelper().insert(f);
                Alert('Berhasil Difavoritkan').show(context);
              },
              child: Icon(
                Icons.favorite_border,
                color: Colors.white,
              )),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              title: Text('Name: ${o.name}'),
            ),
            ListTile(title: Text('Height: ${o.height}')),
            ListTile(title: Text('Mass: ${o.mass}')),
            ListTile(title: Text('Hair Color: ${o.hair_color}')),
            ListTile(title: Text('Eye Color: ${o.eye_color}')),
            ListTile(title: Text('Birth Year: ${o.birth_year}')),
            ListTile(title: Text('Gender: ${o.gender}')),
            ListTile(title: Text('Homeworld: ${o.homeworld}')),
            Expanded(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text("Film: "),
                  ),
                  listButton(o, o.films)
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text("Species: "),
                  ),
                  listButton(o, o.species)
                ],
              ),
            ),
            // ListTile(title: Text('Species: ${o.species}')),
            ListTile(title: Text('Vehicles: ${o.vehicles}')),
            ListTile(title: Text('Starship: ${o.starship}')),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditFormPerson(o)));
              },
              style: ElevatedButton.styleFrom(minimumSize: Size(50.0, 50.0)),
              child: Text('Ubah'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () async {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Icon(Icons.warning),
                        content: Text('Apakah Anda Yakin Menghapus Data Ini?'),
                        actions: [
                          ElevatedButton(
                            child: Text('Tidak'),
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                          ),
                          ElevatedButton(
                            style:
                                ElevatedButton.styleFrom(primary: Colors.red),
                            child: Text('Ya'),
                            onPressed: () async {
                              await Person().delete(o.url);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home()));
                            },
                          )
                        ],
                      );
                    });
              },
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(50.0, 50.0), primary: Colors.red),
              child: Text('Hapus'),
            ),
          ),
        ],
      ),
    );
  }
}
