import 'package:flutter/material.dart';
import 'package:majoo_test/db/personHelper.dart';
import 'package:majoo_test/model/person.dart';
import 'package:majoo_test/view/public/home.dart';

class FormPerson extends StatelessWidget {

  final _formKey = GlobalKey<FormState>();
  Map<String, dynamic> data = new Map();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tambahkan Data Person')),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Nama'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Nama';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['name'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Height'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Height';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['height'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Mass'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Mass';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['mass'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Hair Color'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Hari Color';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['hair_color'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Eye Color'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Eye Color';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['eye_color'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Birth Year'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Birth Year';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['birth_year'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Gender'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Gender';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['gender'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Homeworld'),

                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Homeworld';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['homeworld'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Films'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Films';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['films'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Species'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Species';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['species'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Vehicles'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan vehicles';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['vehicles'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Starship'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Starship';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['starship'] = value;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Masukkan Url'),
                  validator: (value) {
                    if (value == null) {
                      return 'Harap Masukkan Starship';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    data['url'] = value;
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                ElevatedButton(
                  onPressed: () async{

                    if(_formKey.currentState!.validate()){
                        var now = DateTime.now().toString();
                        data['created'] = now;
                        data['edited'] = now;
                        Orang o = Orang.fromMap(data);
                        await Person().insert(o);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Home()));
                    }
                  },
                  child: Text("Tambahkan"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
