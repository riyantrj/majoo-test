import 'package:flutter/material.dart';
import 'package:majoo_test/db/userHelper.dart';
import 'package:majoo_test/main.dart';
import 'package:majoo_test/model/user.dart';
import 'package:majoo_test/sp/sp.dart';

class Profil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(

          child: FutureBuilder(
              future: SP().cache(),
              builder: (context, AsyncSnapshot cache) {
                if (cache.hasData) {
                  var id = cache.data;
                  print(id);
                  return FutureBuilder(
                      future: User().getprofil(id),
                      builder: (context, AsyncSnapshot data) {
                        if (data.hasData) {
                          var d = data.data;
                          return ListView.builder(
                              itemCount: d.length,
                              itemBuilder: (context, i) {
                                Pengguna p = Pengguna.fromMap(d[i]);
                                return ListTile(
                                  leading: Icon(Icons.person),
                                  title: Text('${p.nama}'),
                                  subtitle: Text('${p.username}'),
                                );
                              });
                        } else if (data.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return Container();
                        }
                      });
                } else {
                  return Container();
                }
              }),
        ),
        ElevatedButton(onPressed: () {
           SP().hapus();
           Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Myapp()));
        }, child: Text('Keluar')),
        Padding(padding: EdgeInsets.only(top: 520.0))
      ],
    );
  }
}
