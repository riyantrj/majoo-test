import 'package:flutter/material.dart';
import 'package:majoo_test/model/person.dart';
import 'package:majoo_test/view/partial/detailview.dart';

Widget Listviewbuilder(data, s){
  return ListView.builder(
    itemCount: data.length,
    itemBuilder: (context, index) {
      if(data[index]['name'].toString().toLowerCase().contains(s)){
        Orang o = Orang.fromMap(data[index]);
        return GestureDetector(
          onTap: (){
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Detailveiw(o)));
          },
          child: ListTile(
            leading: Icon(Icons.person),
            trailing: GestureDetector(
                onTap: (){
                  print('tap Disini');
                },
                child: Icon(Icons.favorite)),
            title: Text("Name: ${o.name}"),
            subtitle: Text("Gender: ${o.gender}"),
          ),
        );
      }else{
        return Container();
      }

    },
  );
}