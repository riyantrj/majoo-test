import 'package:flutter/material.dart';
import 'package:majoo_test/model/person.dart';
import 'package:majoo_test/view/partial/film.dart';

Widget listButton(Orang s, string) {
  var listnya = s.balikkanKeList(string);
  // print(listnya);
  // print(listnya.length > 0);
  // return Container();
  if(listnya != null) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: listnya.length,
        itemBuilder: (context, i) {
          return TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Film(listnya[i])));
              },
              child: Container(width: 2.0, child: Text('${i + 1}')));
        });
  }else{
    return Container(child: Text('Tidak Ada'),);
  }
}
