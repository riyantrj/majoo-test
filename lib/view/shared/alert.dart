import 'package:flutter/material.dart';

class Alert {
  final msg;
  Alert(this.msg);

  show(context){
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Icon(Icons.warning),
        content: Text('$msg'),
      );
    });
  }
  close(context){
    Navigator.pop(context);
  }

  showWithButton(context, button, perintah){
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Icon(Icons.warning),
        content: Text('$msg'),
        actions: [
          ElevatedButton(
            child: Text('$button'),
            onPressed: (){
              Navigator.pop(context);
            },
          )
        ],
      );
    });
  }

  showWithButtonTemplate(context, button){
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Icon(Icons.warning),
        content: Text('$msg'),
        actions: [
          ElevatedButton(
            child: Text('$button'),
            onPressed: (){
              Navigator.pop(context);
            },
          )
        ],
      );
    });
  }

  wait(context){
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Text('$msg'),
        content: LinearProgressIndicator(),
      );
    });
  }
}
