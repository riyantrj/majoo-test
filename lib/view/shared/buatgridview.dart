import 'package:flutter/material.dart';
import 'package:majoo_test/model/person.dart';
import 'package:majoo_test/view/partial/detailview.dart';

Widget Gridviewbuilder(data, s) {
  return GridView.builder(
    itemCount: data.length,
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    itemBuilder: (BuildContext context, int index) {
      if (data[index]['name'].toString().toLowerCase().contains(s)) {
        Orang o = Orang.fromMap(data[index]);
        return GestureDetector(
          onTap: () {
            // pass;
            print('tap');
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => Detailveiw(o)));
          },
          child: Card(
            child: GridTile(
              footer: GridTileBar(
                backgroundColor: Colors.black45,
                title: new Text(o.name),
              ),
              child: Icon(Icons.person),
            ),
          ),
        );
      } else {
        return Container();
      }
    },
  );
}
