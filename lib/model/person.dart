

class Orang{
  late String name;
  late String height;
  late String mass;
  late String hair_color;
  late String eye_color;
  late String birth_year;
  late String gender;
  late String homeworld ;
  late String films;
  late String species;
  late String vehicles;
  late String starship;
  late String created;
  late String edited;
  late String url;

 Orang({
   required this.name,
   required this.height,
   required this.mass,
   required this.hair_color,
   required this.eye_color,
   required this.birth_year,
   required this.gender,
   required this.homeworld,
   required this.films,
   required this.species,
   required this.vehicles,
   required this.starship,
   required this.created,
   required this.edited,
   required this.url
});

  Map<String, Object?> toMap(){
    var map = <String, Object?>{
      'name':name,
      'height':height,
      'mass':mass,
      'hair_color':hair_color,
      'eye_color':eye_color,
      'birth_year':birth_year,
      'gender':gender,
      'homeworld':homeworld,
      'films':films,
      'species':species,
      'vehicles':vehicles,
      'starship':starship,
      'created':created,
      'edited':edited,
      'url':url,
    };


    return map;
  }



  Orang.fromMap(Map<String, dynamic> map){
    name =map['name'];
    height = map['height'];
    mass = map['mass'];
    hair_color = map['hair_color'];
    eye_color = map['eye_color'];
    birth_year = map['birth_year'];
    gender = map['gender'];
    homeworld = map['homeworld'].toString();
    films = map['films'].toString();
    species = map['species'].toString();
    vehicles = map['vehicles'].toString();
    starship = map['starship'].toString();
    created = map['created'];
    edited = map['edited'];
    url= map['url'];
  }

  balikkanKeList(s){
    String ws = s.replaceAll('[', '').replaceAll(']', '').replaceAll(' ','');
    List<String> w = ws.split(',');
    if(w[0].isEmpty){
      return null;
    }
    return w;
  }

}