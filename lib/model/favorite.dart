import 'package:majoo_test/db/personHelper.dart';

class FavoriteModel{
  late String id_person;
  late String url;

  FavoriteModel(this.id_person, this.url);

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'id_person':id_person,
      'url': url
    };
    return map;
  }

  FavoriteModel.fromMap(Map<String, dynamic> map){
    id_person = map['id_person'];
    url = map['url'];
  }

  getnama(url) async{
    var d = await Person().getonedata(url);
    return d[0];
  }

}