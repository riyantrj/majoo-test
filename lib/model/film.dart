class FilmModel{
  late String title;
  late int episode_id;
  late String opening_crawl;
  late String director;
  late String producer;
  late String release_date;

  FilmModel(this.title, this.episode_id, this.opening_crawl, this.director, this.producer, this.release_date);

  FilmModel.fromMap(Map<String, dynamic> map){
    title = map['title'];
    episode_id = map['episode_id'];
    opening_crawl = map['opening_crawl'];
    director = map['director'];
    producer = map['producer'];
    release_date = map['release_date'];
  }
}