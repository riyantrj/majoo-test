class Pengguna{
  String username = '';
  String password = '';
  String nama = '';

  Pengguna(this.username, this.nama, this.password);

  Map<String, Object?> toMap(){
    var map = <String, Object?>{
      'username': username,
      'password': password,
      'nama': nama
    };


    return map;
  }



  Pengguna.fromMap(Map<String, dynamic> map){
    nama = map['nama'];
    username = map['username'];
    password = map['password'];

  }
}