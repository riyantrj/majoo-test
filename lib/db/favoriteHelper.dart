import 'package:majoo_test/model/favorite.dart';
import 'package:majoo_test/sp/sp.dart';
import 'package:sqflite/sqflite.dart';

import 'dbhelper.dart';

class FavoriteHelper{
  final tableName = 'favorite';

  createTable() async{
    Database db = await DB().getdb();
    await db.execute('Create Table IF NOT EXISTS favorite  (id INTEGER PRIMARY KEY autoincrement, id_person TEXT, url TEXT)');
  }

  insert(FavoriteModel f) async{
    await createTable();
    Database db = await DB().getdb();
    int id= await db.insert(tableName, f.toMap());
    return id;
  }

  delete(id) async{
    await createTable();
    Database db = await DB().getdb();
    return await db.delete(tableName, where: 'id = ?', whereArgs:[id]);
  }

  getdata() async{
    await createTable();
    Database db = await DB().getdb();
    var id = await SP().cache();
    List<Map<String, dynamic>> data = await db.query(tableName, where: 'id_person = ?', whereArgs: [id]);
    if(data.length > 0){
      return data;
    }else{
      return null;
    }
  }
}