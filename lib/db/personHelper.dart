import 'package:majoo_test/api/api.dart';
import 'package:majoo_test/model/person.dart';
import 'package:sqflite/sqflite.dart';

import 'dbhelper.dart';

class Person{
  final tableName = 'person';


  createTable() async{
    Database db = await DB().getdb();
    await db.execute('Create Table IF NOT EXISTS person  (id INTEGER PRIMARY KEY autoincrement, '
        'name varchar(200), height varchar(5), mass varchar(5), hair_color varchar(10), skin_color varchar(10), eye_color varchar(10),'
        'birth_year varchar(10), gender varchar(10), homeworld text, films text, species text, vehicles text, starship text, created text,'
        'edited text, url text unique)');
  }

  insert(Orang person) async{
    await createTable();
    Database db = await DB().getdb();
    int id= await db.insert(tableName, person.toMap());
    return id;
  }
  update(Orang person) async{
    await createTable();
    Database db = await DB().getdb();
    await db.update(tableName, person.toMap(), where: 'url = ?', whereArgs: [person.url]);
  }

  getdata({sort:'ASC'}) async{
    await createTable();
    Database db = await DB().getdb();
    List<Map<String, dynamic>> data = await db.query(tableName, orderBy: 'name $sort, created DESC');
    if(data.length > 0){
      return data;
    }else{
      return null;
    }
  }

  getonedata(url) async{
    await createTable();
    Database db = await DB().getdb();
    List<Map<String, dynamic>> data = await db.query(tableName, where: 'url = ?', whereArgs: [url]);
    if(data.length > 0){
      return data;
    }else{
      return null;
    }
  }

  delete(url) async{
    await createTable();
    Database db = await DB().getdb();
    return await db.delete(tableName, where: 'url = ?', whereArgs:[url]);
  }


}