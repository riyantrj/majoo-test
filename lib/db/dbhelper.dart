
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DB{


  Future<Database> getdb() async{

    Database _db = await initDB();
    return _db;
  }

  initDB() async{
    var dbpath = await getDatabasesPath();
    String path = join(dbpath, 'majoo.db');
    var db = await openDatabase(path, version: 1,);
    return db;
  }
}