
import 'package:majoo_test/db/dbhelper.dart';
import 'package:majoo_test/model/user.dart';
import 'package:majoo_test/sp/sp.dart';
import 'package:sqflite/sqflite.dart';


class User{
  final tableName = 'user';


  createTable() async{
    Database db = await DB().getdb();
    await db.execute('Create Table IF NOT EXISTS user  (id INTEGER PRIMARY KEY autoincrement, nama TEXT, username TEXT UNIQUE, password TEXT)');
  }

  insert(Pengguna user) async{
    await createTable();
    Database db = await DB().getdb();
    int id= await db.insert(tableName, user.toMap());
    return id;
  }

  login(username, password) async{
    await createTable();
    Database db = await DB().getdb();
    List<Map<String, dynamic>> maps = await db.query(tableName,columns: ['id'], where: 'username=? and password = ?', whereArgs: [username, password]);
    if(maps.length>0){
     return maps[0]['id'];
    }else{
      return null;
    }
  }

  getprofil(id) async{
    await createTable();
    Database db = await DB().getdb();
    List<Map<String, dynamic>> data = await db.query(tableName, where: 'id = ?', whereArgs: [id]);
    if(data.length > 0){
      return data;
    }else{
      return null;
    }
  }

}