import 'package:flutter/material.dart';
import 'package:majoo_test/sp/sp.dart';
import 'package:majoo_test/view/public/home.dart';
import 'package:majoo_test/view/public/login.dart';
import 'package:majoo_test/view/public/register.dart';

void main() {
  runApp(Myapp());
}

class Myapp extends StatefulWidget {
  @override
  _MyappState createState() => _MyappState();
}

class _MyappState extends State<Myapp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(
        future: SP().cache(),
        builder: (context, data){
          print(data.data);
          if(data.hasData){
           return Home();
          }else{
            return Login();
          }
          return CircularProgressIndicator();

        },
      ),
      // home: Register(),
    );
  }
}
